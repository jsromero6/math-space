# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ventanaPrincipal(object):
    def setupUi(self, ventanaPrincipal):
        ventanaPrincipal.setObjectName("ventanaPrincipal")
        ventanaPrincipal.setWindowModality(QtCore.Qt.ApplicationModal)
        ventanaPrincipal.resize(767, 413)
        ventanaPrincipal.setStyleSheet("background-color: rgb(0, 85, 127);")
        self.centralwidget = QtWidgets.QWidget(ventanaPrincipal)
        self.centralwidget.setObjectName("centralwidget")
        self.lblTitulo = QtWidgets.QLabel(self.centralwidget)
        self.lblTitulo.setGeometry(QtCore.QRect(300, 20, 261, 41))
        font = QtGui.QFont()
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.lblTitulo.setFont(font)
        self.lblTitulo.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lblTitulo.setScaledContents(True)
        self.lblTitulo.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTitulo.setWordWrap(True)
        self.lblTitulo.setIndent(0)
        self.lblTitulo.setObjectName("lblTitulo")
        self.btnAyuda = QtWidgets.QPushButton(self.centralwidget)
        self.btnAyuda.setGeometry(QtCore.QRect(697, 20, 61, 34))
        self.btnAyuda.setObjectName("btnAyuda")
        self.btnPantCompleta = QtWidgets.QPushButton(self.centralwidget)
        self.btnPantCompleta.setGeometry(QtCore.QRect(597, 20, 91, 34))
        self.btnPantCompleta.setObjectName("btnPantCompleta")
        self.btnComenzar = QtWidgets.QPushButton(self.centralwidget)
        self.btnComenzar.setGeometry(QtCore.QRect(20, 210, 101, 34))
        self.btnComenzar.setObjectName("btnComenzar")
        self.btnConfiguracion = QtWidgets.QPushButton(self.centralwidget)
        self.btnConfiguracion.setGeometry(QtCore.QRect(20, 260, 101, 34))
        self.btnConfiguracion.setObjectName("btnConfiguracion")
        self.btnSalir = QtWidgets.QPushButton(self.centralwidget)
        self.btnSalir.setGeometry(QtCore.QRect(20, 310, 101, 34))
        self.btnSalir.setObjectName("btnSalir")
        self.lblPersonajes = QtWidgets.QLabel(self.centralwidget)
        self.lblPersonajes.setGeometry(QtCore.QRect(610, 120, 81, 18))
        self.lblPersonajes.setObjectName("lblPersonajes")
        ventanaPrincipal.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ventanaPrincipal)
        self.statusbar.setObjectName("statusbar")
        ventanaPrincipal.setStatusBar(self.statusbar)

        self.retranslateUi(ventanaPrincipal)
        QtCore.QMetaObject.connectSlotsByName(ventanaPrincipal)

    def retranslateUi(self, ventanaPrincipal):
        _translate = QtCore.QCoreApplication.translate
        ventanaPrincipal.setWindowTitle(_translate("ventanaPrincipal", "Math-Space"))
        self.lblTitulo.setText(_translate("ventanaPrincipal", "MATH -SPACE"))
        self.btnAyuda.setText(_translate("ventanaPrincipal", "Ayuda"))
        self.btnPantCompleta.setText(_translate("ventanaPrincipal", "P. Completa"))
        self.btnComenzar.setText(_translate("ventanaPrincipal", "Comenzar"))
        self.btnConfiguracion.setText(_translate("ventanaPrincipal", "Configuración"))
        self.btnSalir.setText(_translate("ventanaPrincipal", "Salir"))
        self.lblPersonajes.setText(_translate("ventanaPrincipal", "Personajes"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ventanaPrincipal = QtWidgets.QMainWindow()
    ui = Ui_ventanaPrincipal()
    ui.setupUi(ventanaPrincipal)
    ventanaPrincipal.show()
    sys.exit(app.exec_())
