Perfil de proyecto

Tema: Operaciones con números decimales y fracciones.

Objetivo general.
Elaborar actividades que nos permitan evaluar los conocimientos que han adquirido los estudiantes con el recurso referente a las operaciones con números decimales y fracciones enfocado en los estudiantes de séptimo año de la Escuela de Educación Básica “Mons. Juan María Riofrío”, utilizando para ello el lenguaje de programación Python, con la librería de Pygame.

Objetivos específicos.
•	Elaborar actividades digitales lúdicas mediante el lenguaje de programación Python, usando la librería Pygame.
•	Cuantificar los aciertos y fallos del estudiante a lo largo de las actividades para poder obtener un resultado cuantificable y medible.
•	Comprobar si el estudiante ha sido capaz de dominar los conocimientos básicos, por medio de una evaluación sumativa.

Alcance:
Los usuarios serán capaces de dominar los conceptos, elementos de la división, razonamiento lógico-matemático y la resolución de problemas matemáticos centrados en la división pero que indirectamente practicarán operaciones como la resta, suma y multiplicación.

Metodología:
Implementación de actividades digitales lúdicas que nos permitan mediante un método cuantificable medir el nivel de conocimientos que presenta el estudiante con respecto a la temática.
    Fase Inicial
    En base a los recursos realizados el ciclo anterior para el área de matemáticas del séptimo año de Educación General Básica (EGB) de la Escuela “Mons. Juan María Riofrio”, abordaremos a los temas en un conjunto de evaluaciones formativas y sumativas elaborada bajo el lenguaje de programación Python; haciendo uso de la librería Pygame.
    
    Desarrollo
    Para el desarrollo del siguiente proyecto se utilizará la metodología “Agile” ya que permite trabajar de manera rápida y flexible, adaptando mejoras y cambios dentro del recurso a lo largo del desarrollo del mismo, debido a las características que nos presenta esta metodología:
    •	Iteración. Es un ciclo de tiempo que se va repitiendo. Tras cada iteración, el producto o servicio gana valor. Este se revisa hasta que el cliente queda conforme, por lo que es frecuente facturar por iteración.
    •	Inspección. Tras cada iteración el producto se revisa por el proveedor y el cliente. Juntos, determinan si merece la pena una iteración más. El feedback (comentarios críticos y puntos de mejora) es clave.
    •	Adaptación. Cabe la posibilidad de que el objetivo cambie, o bien el modo en que lo alcanzamos. Hay que estar abiertos a cambiar de dirección y a la mejora continua.
    •	Mejora continua. También es una filosofía del trabajo que parte del concepto de que todo proceso, producto o servicio puede mejorarse. O, dicho de otro modo, que uno siempre está lo suficientemente equivocado como para mejorar.
    El proceso a seguir consta de las siguientes fases:
    •	Analizar los contenidos previamente incorporados en los recursos educativos para proceder a elaborar actividades evaluativas sobre las temáticas abordadas.
    •	Crear actividades de evaluación interactivas, las cuales deben reflejar la comprensión de los contenidos y procesos de resolución de problemas matemáticos.
    •	Seguir un proceso de evaluación del RED con los expertos para la rectificación y aprobación del mismo.
    Luego de cada iteración se revisa que se debe cambiar y mejorar para lograr la mejora continua en las sucesivas iteraciones.
    
    Compartir
    El presente recurso se colocará en un repositorio GitLab (https://gitlab.com/jsromero6/math-space), para que sea de libre acceso y pueda ser descargado y/o realizar sus propias versiones según las necesidades del usuario.
